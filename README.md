Top Level Scraper
===

This script will pull all of the top level comments from a submission and display them.

Instructions:

1. Open TLS.py.
2. Edit 'username' to be whatever username you wish to use.
   Example: username = 'PixelOrange'
3. Edit 'password' to be whatever password is associated with that account. CASE SENSITIVE!.
   Example: password = 'ChuckyCheese'
4. Run the script like you would any other python script.
5. When prompted, enter the full link of whatever submission you want to scrape.
6. Be patient!
7. Enjoy your results.

---

Requirements:

Python

PRAW
