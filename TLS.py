# encoding: utf-8

# Imports

import praw
import re

# Variables

username = 'username'
password = 'password'

basic_info = username + " TopLevelScraper - Visit https://github.com/PixelOrange/TLS/ for more information."
r = praw.Reddit(basic_info)

# Functions

def try_login():
# Tries to log in. If there is an error it should kill the app.
    try:
        r.login(username, password)
        print "Login Successful \n"
    except:
        print "Login Failed"
        raise SystemExit(0)

def grab_submission(sub_id):
# Grabs a single submission. This is currently for testing purposes only.
    sub_id = re.search('(/comments/)(\w+)', sub_id) 
    try:
        print "This may take some time. Be patient.\n"
        submission = r.get_submission(submission_id=sub_id.group(2))
        submission.replace_more_comments(limit=None, threshold=0)
    except:
        print "Failed to acquire submission. Trying again."
    comments = submission.comments
    print "Submission: %s\n" % submission.title
    for comment in comments:
        print "Commenter: %s\nComment: %s\n\n" % (comment.author, comment)

# Commands

print "\nLogging into Reddit\n"
try_login()

if r.is_logged_in():
    sub_id = raw_input("Enter your submission link: ")
    grab_submission(sub_id)
